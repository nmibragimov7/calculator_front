import {
    FETCH_TARIFFS_SUCCESS, 
    CREATE_TARIFF_SUCCESS, 
    CREATE_TARIFF_FAILURE, 
    REMOVE_TARIFF_SUCCESS,
    REMOVE_TARIFF_FAILURE,
    CALCULATE_TARIFF_SUCCESS,
    CALCULATE_TARIFF_FAILURE,
    RESET_ERROR,
    RESET_MESSAGE,
    RESET_TARIFF
} from "../actions/tariffsActions";

const initialState = {
    tariffs: [],
    tariff: {},
    message: null,
    errors: null
};

const tariffsReducer = (state = initialState, action) => {
    switch(action.type){
        case FETCH_TARIFFS_SUCCESS:
            return {...state, tariffs: action.tariffs}
        case CREATE_TARIFF_SUCCESS:
            return {...state, errors: null, message: action.message}
        case CREATE_TARIFF_FAILURE:
            return {...state, errors: action.errors, message: null}
        case REMOVE_TARIFF_SUCCESS:
            return {...state, errors: null, message: action.message}
        case REMOVE_TARIFF_FAILURE:
            return {...state, errors: action.errors, message: null}
        case CALCULATE_TARIFF_SUCCESS:
            return {...state, errors: null, tariff: action.tariff}
        case CALCULATE_TARIFF_FAILURE:
            return {...state, errors: action.errors}
        case RESET_TARIFF:
            return {...state, tariff: {}}
        case RESET_ERROR:
            return {...state, errors: null}
        case RESET_MESSAGE:
            return {...state, message: null}
        default:
            return state;
    }
};

export default tariffsReducer;