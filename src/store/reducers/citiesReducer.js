import {
    FETCH_CITIES_SUCCESS, 
    CREATE_CITY_SUCCESS, 
    CREATE_CITY_FAILURE, 
    REMOVE_CITY_SUCCESS,
    REMOVE_CITY_FAILURE,
    RESET_ERROR,
    RESET_MESSAGE
} from "../actions/citiesActions";

const initialState = {
    cities: [{
        id: 1,
        title: "Almaty"
    }, {
        id: 2,
        title: "Astana"
    }],
    message: null,
    errors: null
};

const citiesReducer = (state = initialState, action) => {
    switch(action.type){
        case FETCH_CITIES_SUCCESS:
            return {...state, cities: action.cities}
        case CREATE_CITY_SUCCESS:
            return {...state, errors: null, message: action.message}
        case CREATE_CITY_FAILURE:
            return {...state, errors: action.errors, message: null}
        case REMOVE_CITY_SUCCESS:
            return {...state, errors: null, message: action.message}
        case REMOVE_CITY_FAILURE:
            return {...state, errors: action.errors, message: action.message}
        case RESET_ERROR:
            return {...state, errors: null}
        case RESET_MESSAGE:
            return {...state, message: null}
        default:
            return state;
    }
};

export default citiesReducer;