import {LOADING_HANDLER} from "../actions/loadingActions";

const initialState = {
    loading: false
};

const artistsReducer = (state = initialState, action) => {
    switch(action.type){
        case LOADING_HANDLER:
            return {...state, loading: action.loading}
        default:
            return state;
    }
};

export default artistsReducer;