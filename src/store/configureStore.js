import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import loadingReducer from "./reducers/loadingReducer";
import usersReducer from "./reducers/usersReducer";
import {createBrowserHistory} from 'history';
import {connectRouter, routerMiddleware} from 'connected-react-router';
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import citiesReducer from './reducers/citiesReducer';
import tariffsReducer from './reducers/tariffsReducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    loading: loadingReducer,
    users: usersReducer,
    cities: citiesReducer,
    tariffs: tariffsReducer,
    router: connectRouter(history)
});

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];
const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadFromLocalStorage();

export const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(()=> {
    saveToLocalStorage({
        users:{
            user: store.getState().users.user
        }
    });
});