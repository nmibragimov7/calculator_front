import axiosApi from "../../axiosApi";
import {loadingOffHandler} from "./loadingActions";

export const FETCH_TARIFFS_SUCCESS = 'FETCH_TARIFFS_SUCCESS';
export const CREATE_TARIFF_SUCCESS = 'CREATE_TARIFF_SUCCESS';
export const CREATE_TARIFF_FAILURE = 'CREATE_TARIFF_FAILURE';
export const REMOVE_TARIFF_SUCCESS = 'REMOVE_TARIFF_SUCCESS';
export const REMOVE_TARIFF_FAILURE = 'REMOVE_TARIFF_FAILURE';
export const CALCULATE_TARIFF_SUCCESS = 'CALCULATE_TARIFF_SUCCESS';
export const CALCULATE_TARIFF_FAILURE = 'CALCULATE_TARIFF_FAILURE';
export const RESET_ERROR = 'RESET_ERROR';
export const RESET_MESSAGE = 'RESET_MESSAGE';
export const RESET_TARIFF = 'RESET_TARIFF';
 
const fetchTariffsSuccess = tariffs => {
    return {type: FETCH_TARIFFS_SUCCESS, tariffs}
};

export const resetError = () => {
    return {type: RESET_ERROR}
};

export const resetMessage = () => {
    return {type: RESET_MESSAGE}
};

const createTariffSuccess = (message) => {
    return {type: CREATE_TARIFF_SUCCESS, message}
};

const createTariffFailure = (errors) => {
    return {type: CREATE_TARIFF_FAILURE, errors}
};

const removeTariffSuccess = (message) => {
    return {type: REMOVE_TARIFF_SUCCESS, message}
};

const removeTariffFailure = (errors) => {
    return {type: REMOVE_TARIFF_FAILURE, errors}
};

const calculateTariffSuccess = (tariff) => {
    return {type: CALCULATE_TARIFF_SUCCESS, tariff}
};

const calculateTariffFailure = (errors) => {
    return {type: CALCULATE_TARIFF_FAILURE, errors}
};

export const resetTariff = () => ({type: RESET_TARIFF});

export const fetchTariffs = () => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/tariffs');
            dispatch(fetchTariffsSuccess(response.data));
            // dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};

export const createTariff = (tariff) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            const response = await axiosApi.post(`/tariffs`, tariff, {headers});
            dispatch(createTariffSuccess(response.data));
            dispatch(fetchTariffs());
        } catch(e) {
            dispatch(createTariffFailure(e.response.data));
            console.error(e);
        }
    }
};

export const removeTariff = (id) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            const response = await axiosApi.delete(`/tariffs/${id}`, {headers});
            dispatch(removeTariffSuccess(response.data));
            dispatch(fetchTariffs());
        } catch(e) {
            dispatch(removeTariffFailure(e.response.data));
            console.error(e);
        }
    }
};

export const calculateTariff = (data) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            const response = await axiosApi.post(`/tariffs/calculate`, data, {headers});
            dispatch(calculateTariffSuccess(response.data));
        } catch(e) {
            dispatch(calculateTariffFailure(e.response.data));
            console.error(e);
        }
    }
};