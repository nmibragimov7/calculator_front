import axiosApi from "../../axiosApi";
import {loadingOffHandler} from "./loadingActions";

export const FETCH_CITIES_SUCCESS = 'FETCH_CITIES_SUCCESS';
export const CREATE_CITY_SUCCESS = 'CREATE_CITY_SUCCESS';
export const CREATE_CITY_FAILURE = 'CREATE_CITY_FAILURE';
export const REMOVE_CITY_SUCCESS = 'REMOVE_CITY_SUCCESS';
export const REMOVE_CITY_FAILURE = 'REMOVE_CITY_FAILURE';
export const RESET_ERROR = 'RESET_ERROR';
export const RESET_MESSAGE = 'RESET_MESSAGE';
 
const fetchCitiesSuccess = cities => {
    return {type: FETCH_CITIES_SUCCESS, cities}
};

export const resetError = () => {
    return {type: RESET_ERROR}
};

export const resetMessage = () => {
    return {type: RESET_MESSAGE}
};

const createCitySuccess = (message) => {
    return {type: CREATE_CITY_SUCCESS, message}
};

const createCityFailure = (errors) => {
    return {type: CREATE_CITY_FAILURE, errors}
};

const removeCitySuccess = (message) => {
    return {type: REMOVE_CITY_SUCCESS, message}
};

const removeCityFailure = (errors) => {
    return {type: REMOVE_CITY_FAILURE, errors}
};

export const fetchCities = () => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/cities');
            dispatch(fetchCitiesSuccess(response.data));
            // dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};

export const createCity = (city) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            const response = await axiosApi.post(`/cities`, city, {headers});
            dispatch(createCitySuccess(response.data));
            dispatch(fetchCities());
        } catch(e) {
            dispatch(createCityFailure(e.response.data));
            console.error(e);
        }
    }
};

export const removeCity = (id) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            const response = await axiosApi.delete(`/cities/${id}`, {headers});
            dispatch(removeCitySuccess(response.data));
            dispatch(fetchCities());
        } catch(e) {
            dispatch(removeCityFailure(e.response.data));
            console.error(e);
        }
    }
};