import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories, fetchProducts, resetMessage} from "../../store/actions/productsActions";
import {loadingHandler} from "../../store/actions/loadingActions";
import Porduct from "../../components/Porduct/Porduct";
import Spinner from "../../components/Spinner/Spinner";
import ListBlock from '../../components/UI/ListBlock/ListBlock';
import {makeStyles} from "@material-ui/core/styles";
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    }
}));

const Porducts = (props) => {
    const classes = useStyles();

    const [categoryName, setCategoryName] = useState("All products");
    const dispatch = useDispatch();
    const categories = useSelector(state => state.products.categories);
    const products = useSelector(state => state.products.products);
    const message = useSelector(state => state.products.message);
    const loading = useSelector(state => state.loading.loading);

    useEffect(()=> {
        dispatch(fetchCategories());
        dispatch(loadingHandler());
        dispatch(fetchProducts(null));
    }, [dispatch]);

    const blockHandler = (id) => {
        props.history.push({
            pathname: `/${id}`,
        });
    };

    const closeAlert = () => {
        dispatch(resetMessage());
    }

    const fetchProductsByCategory = (category) => {
        dispatch(loadingHandler());
        dispatch(fetchProducts(category));

        setCategoryName("All products");
        if(category) {
            const index = categories.findIndex(item => item._id === category);
            setCategoryName(categories[index].category);
        } 
    };

    let form = (
        <>
            {products.map(product => (
                <Grid item key={product._id}>
                    <Porduct
                    key={product._id}
                    title={product.title}
                    image={product.image}
                    price={product.price}
                    blockHandler={() => blockHandler(product._id)}
                    />  
                </Grid>
            ))}
        </>
    );

    if (loading) {
        form = <Spinner />;
    }

    return (
        <>
            {message && message.message &&
                <Alert 
                onClose={closeAlert}
                severity="success"
                className={classes.alert}
                >
                    {message.message}
                </Alert>
            }
            <Grid 
            container 
            direction="row"
            justify="space-between"
            alignItems="flex-start"
            spacing={2}>
                <Grid 
                item 
                xs={12} 
                sm={3}>
                    <ListBlock
                    categories={categories}
                    fetchProductsByCategory={fetchProductsByCategory}
                    />
                </Grid>
                <Grid 
                item 
                xs={12} 
                sm={9}
                container 
                >
                    <h1>{categoryName}</h1>
                    <Grid 
                    item 
                    container 
                    direction="row"
                    alignItems="center"
                    spacing={2}
                    >
                        {form}

                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default Porducts;