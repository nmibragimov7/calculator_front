import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from "@material-ui/core/styles";
import Alert from '@material-ui/lab/Alert';
import CalculatorForm from '../../components/UI/CalculatorForm/CalculatorForm';
import { fetchCities } from '../../store/actions/citiesActions';
import { calculateTariff, resetTariff, resetError } from '../../store/actions/tariffsActions';

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    }
}));

const Calculator = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const errors = useSelector(state => state.tariffs.errors);
    const cities = useSelector(state => state.cities.cities);
    const tariff = useSelector(state => state.tariffs.tariff.tariff);

    useEffect(()=> {
        dispatch(fetchCities());
        dispatch(resetError());
        dispatch(resetTariff());
    }, [dispatch]);

    const CalculatorHandler = (data) => {
        dispatch(calculateTariff(data));
    };
    const closeAlert = () => {
        dispatch(resetError());
    };

    return (
        <>
            {errors && errors.error &&
                <Alert 
                onClose={closeAlert}
                severity="error"
                className={classes.alert}
                >
                    {errors.error}
                </Alert>
            }
            <CalculatorForm 
            cities={cities}
            tariff={tariff}
            onSubmit={CalculatorHandler}
            />
        </>
    );
};

export default Calculator;