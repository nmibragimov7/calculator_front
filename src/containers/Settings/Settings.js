import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import TableForm from '../../components/UI/Form/TableForm';
import AddCityForm from '../../components/UI/AddCityForm/AddCityForm';
import { Grid } from '@material-ui/core';
import AddTariffForm from '../../components/UI/AddTariffForm/AddTariffForm';
import { fetchCities, createCity, removeCity } from '../../store/actions/citiesActions';
import { fetchTariffs, createTariff, removeTariff } from '../../store/actions/tariffsActions';

const Settings = () => {
    const dispatch = useDispatch();
    const cities = useSelector(state => state.cities.cities);
    const tariffs = useSelector(state => state.tariffs.tariffs);

    useEffect(()=> {
        dispatch(fetchCities());
        dispatch(fetchTariffs());
    }, [dispatch]);

    const addCityHandler = (city) => {
        dispatch(createCity(city));
    };
    const removeCityHandler = (id) => {
        dispatch(removeCity(id));
    };
    const addTariffHandler = (tariff) => {
        dispatch(createTariff(tariff));
    };
    const removeTariffHandler = (id) => {
        dispatch(removeTariff(id));
    };

    return (
        <>
            <Grid container direction="column" spacing={2}>
                <Grid item>
                    <h3>Cities:</h3>
                    <TableForm 
                    data={cities} 
                    table="cities" 
                    removeHandler={removeCityHandler}
                    />
                </Grid>
                <Grid item>
                    <AddCityForm onSubmit={addCityHandler}/>
                </Grid>
            </Grid>
            <Grid container direction="column" spacing={2}>
                <Grid item>
                    <h3>Tariffs:</h3>
                    <TableForm 
                    data={tariffs} 
                    table="tariffs" 
                    removeHandler={removeTariffHandler}
                    />
                </Grid>
                <Grid item>
                    <AddTariffForm 
                    cities={cities}
                    onSubmit={addTariffHandler}
                    />
                </Grid>
            </Grid>            
        </>
    );
};

export default Settings;