import React from 'react';
import Container from "@material-ui/core/Container";
import {Route, Switch, Redirect} from "react-router-dom"
import AppToolbar from "./components/AppToolbar/AppToolbar";
import Register from "./containers/User/Register";
import Login from "./containers/User/Login";
import {useSelector} from "react-redux";
import Calculator from './containers/Calculator/Calculator';
import Settings from './containers/Settings/Settings';

const App = () => {
  const user = useSelector(store => store.users.user);

  return(
    <>
      <header><AppToolbar user={user}/></header>
      <main>
        <Container>
          <Switch>
            <Route path="/" exact component={Calculator}/>
            <ProtectRoute path="/settings" isAllowed={user} redirectTo={'/'} exact component={Settings}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path='/' render={()=>(<div><h1>404 Not found</h1></div>)}/>
          </Switch>
        </Container>
      </main>
    </>
  );
};

const ProtectRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
      <Route {...props} /> :
      <Redirect to={redirectTo} />
};

export default App;
