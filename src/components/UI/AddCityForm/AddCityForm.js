import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import {useSelector} from "react-redux";
import TextInput from "../Form/TextInput";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2),
        padding: "20px",
        border: "1px solid rgb(187, 195, 204)",
        background: "rgb(248, 249, 250)",
    }
}));

const AddCityForm = (props) => {
    const classes = useStyles();
    const [state, setState] = useState({
        title: "",
    });

    const errors = useSelector(state => state.cities.errors);

    const inputChangeHandler = event => {
        const {name, value} = event.target;
        setState(prevState => {
            return {...prevState, [name]: value}
        })
    };
    
    const submitFormHandler = event => {
        event.preventDefault();
        props.onSubmit(state);
    };

    const getFieldError = fieldName => {
        try {
            return errors.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <form
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
            <Grid container direction="column" spacing={2}>
                <Grid item>
                    <TextInput
                        label="title"
                        onChange={inputChangeHandler}
                        name="title"
                        error={getFieldError('title')}
                        required={true}
                    />
                </Grid>
                <Grid item>
                    <Button type="submit" color="primary" variant="contained">Add new city</Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default AddCityForm;