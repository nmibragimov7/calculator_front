import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import {useSelector} from "react-redux";
import SelectCategory from '../Form/SelectCategory';
import TextInput from '../Form/TextInput';
import { TextField } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2),
        padding: "20px",
        border: "1px solid rgb(187, 195, 204)",
        background: "rgb(248, 249, 250)",
    }
}));

const CalculatorForm = (props) => {
    const classes = useStyles();
    const [state, setState] = useState({
        locationFrom: "",
        locationTo: "",
        weight: "",
    });

    const errors = useSelector(state => state.cities.errors);

    const inputChangeHandler = event => {
        const {name, value} = event.target;
        setState(prevState => {
            return {...prevState, [name]: value}
        })
    };
    
    const submitFormHandler = event => {
        event.preventDefault();
        props.onSubmit(state);
    };
    
    const getFieldError = fieldName => {
        try {
            return errors.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };
    
    return (
        <form
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
            <Grid container direction="column" spacing={2}>
                <Grid item>
                    <SelectCategory
                        input={state.locationFrom}
                        title="From"
                        name="locationFrom"
                        cities={props.cities}
                        inputChangeHandler={inputChangeHandler}
                    />
                </Grid>
                <Grid item>
                    <SelectCategory
                        input={state.locationTo}
                        title="To"
                        name="locationTo"
                        cities={props.cities}
                        inputChangeHandler={inputChangeHandler}
                    />
                </Grid>
                <Grid item>
                    <TextInput
                        label="weight"
                        onChange={inputChangeHandler}
                        name="weight"
                        error={getFieldError('weight')}
                        required={true}
                    />
                </Grid>
                <Grid item>
                    <h3>{props.tariff} KZT</h3>
                </Grid>
                <Grid item>
                    <Button type="submit" color="primary" variant="contained">Calculate</Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default CalculatorForm;