import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: "300px"
    },
}));

 const SelectCategory = (props) => {
    const classes = useStyles();

    return (
        <FormControl required className={classes.formControl}>
            <InputLabel id="category-native-required">{props.title}</InputLabel>
            <Select
            native
            value={props.input}
            onChange={props.inputChangeHandler}
            name={props.name}
            inputProps={{
                id: 'category-native-required',
            }}
            >
                <option aria-label="None" value={null} />
                {props.cities.map((city, index) => (
                    <option key={index} value={city._id}>{city.title}</option>
                ))}
            </Select>
            <FormHelperText>Required</FormHelperText>
        </FormControl>
    );
}

export default SelectCategory;