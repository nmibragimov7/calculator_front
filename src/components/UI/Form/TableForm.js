import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Button } from '@material-ui/core';

const useStyles = makeStyles({
    table: {
        width: "100%",
    },
    button: {
        width: "100px",
        marginLeft: "10px"
    }
});

const TableForm = (props) => {
    const classes = useStyles();

    let createData = null;
    if(props.table === "cities") {
        createData = (title, action) => {
            return { title, action };
        }
    } 
    if(props.table === "tariffs") {
        createData = (locationFrom, locationTo, tariff, action) => {
            return { locationFrom, locationTo, tariff, action };
        }
    } 

    let rows = [];
    const pushData = (data) => {
        if(data.length === 2) {
            rows.push(
                createData(data[0], data[1])
            );
        }
        if(data.length === 4) {
            rows.push(
                createData(data[0], data[1], data[2], data[3])
            );
        }
        
    };

    props.data.forEach(item => {
        
        if(props.table === "cities") {
            pushData(
                [
                    item.title, 
                    (
                        <>
                            <Button className={classes.button} variant="contained" color="secondary" onClick={() => {props.removeHandler(item._id)}}>Delete</Button>
                        </>
                    )
                ]
            )
        }
        if(props.table === "tariffs") {
            pushData(
                [
                    (item.locationFrom && item.locationFrom.title), 
                    (item.locationTo && item.locationTo.title), 
                    item.tariff,
                    (
                        <>
                            <Button className={classes.button} variant="contained" color="secondary" onClick={() => {props.removeHandler(item._id)}}>Delete</Button>
                        </>
                    )
                ]
            )
        }
    });

    return (
        <>
            <h1>{props.tableName}</h1>
            <TableContainer component={Paper}>
                <Table className={classes.table} size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow>
                            {props.table === "cities" ? (
                                <>
                                    <TableCell align="center"><b>Title</b></TableCell>
                                    <TableCell align="center"><b>Action</b></TableCell>
                                </>
                            ) : null}
                            {props.table === "tariffs" ? (
                                <>
                                    <TableCell align="center"><b>From</b></TableCell>
                                    <TableCell align="center"><b>To</b></TableCell>
                                    <TableCell align="center"><b>Tariff</b></TableCell>
                                    <TableCell align="center"><b>Action</b></TableCell>
                                </>
                            ) : null}
                            
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {props.table === "cities" ? (
                            <>
                                {rows.map((row, index) => (
                                    <TableRow key={index}>
                                        <TableCell align="center">
                                            {row.title}
                                        </TableCell>
                                        <TableCell align="center">
                                            {row.action}
                                        </TableCell>
                                    </TableRow>
                                ))}  
                            </>  
                        ) : null}
                        {props.table === "tariffs" ? (
                            <>
                                {rows.map((row, index) => (
                                    <TableRow key={index}>
                                        <TableCell align="center">
                                            {row.locationFrom}
                                        </TableCell>
                                        <TableCell align="center">
                                            {row.locationTo}
                                        </TableCell>
                                        <TableCell align="center">
                                            {row.tariff}
                                        </TableCell>
                                        <TableCell align="center">
                                            {row.action}
                                        </TableCell>
                                    </TableRow>
                                ))}  
                            </> 
                        ) : null}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    );
}

export default TableForm;